const search = document.getElementById('search');
const matchList = document.getElementById('match-list');

// Search states.json and filter it
const searchStates = async (searchText) => {
    const res = await fetch('../data/states.json');
    const states = await res.json();

    // Get matches
    let matches = states.filter(state => {
        const regex = new RegExp(`^${searchText}`, 'gi');
        return state.name.match(regex) || state.abbr.match(regex);
    });

    if(searchText.length === 0) {
        matches = [];
        matchList.innerHTML = '';
    }

    outputHTML(matches);
}

// show results in HTML
const outputHTML = matches => {
    if (matches.length > 0) {
        const html = matches.map( match => `
            <div class="match-item">
                <h4>${match.name} (${match.abbr}) <span class="item-capital">${match.capital}</span></h4>
                <small>Lat: ${match.lat} / Long: ${match.long}</small>
            </div>
        `).join('');
        matchList.innerHTML = html;
    }
}

search.addEventListener('input', (e) => searchStates(search.value)); 